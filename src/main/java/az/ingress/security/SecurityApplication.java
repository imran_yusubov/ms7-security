package az.ingress.security;

import az.ingress.security.domain.User;
import az.ingress.security.domain.UserAuthority;
import az.ingress.security.jwt.JwtService;
import az.ingress.security.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.Duration;
import java.util.Set;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class SecurityApplication implements CommandLineRunner {

    private final UserRepository userRepository;
    private final JwtService jwtService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        SpringApplication.run(SecurityApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("Creating demo users");
        User user = new User();
        user.setUsername("hafiz");
        user.setPassword(passwordEncoder().encode("123456"));
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);

        UserAuthority userAuthority = new UserAuthority();
        userAuthority.setAuthority("ROLE_ADMIN");
        user.setAuthorities(Set.of(userAuthority));

        userRepository.save(user);
        String jwt = jwtService.issueToken(user, Duration.ofDays(1));
        log.info("Jwt is : {}", jwt);
        log.info("Claims from JWT : {}", jwtService.parseToken(jwt));

    }
}

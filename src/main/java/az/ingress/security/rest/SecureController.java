package az.ingress.security.rest;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@Slf4j
@RestController
@RequestMapping("/secure")
public class SecureController {

    @GetMapping("/hello")
    public HelloDto helloWorld() {
        return new HelloDto("Welcome user, for login please.");
    }

    @GetMapping("/authenticated")
    public HelloDto helloWorld(Principal principal) {
        return new HelloDto("Welcome " + principal.getName());
    }

    //USER
    @GetMapping("/user")
    public HelloDto helloUser(Principal principal) {
        return new HelloDto("Welcome " + principal.getName() + " this is a secure content that is accessible only to USER role");
    }

    //ADMIN
    @GetMapping("/admin")
    public HelloDto helloAdmin(Principal principal) {
        return new HelloDto("Welcome " + principal.getName() + " this is a secure content that is accessible only to ADMIN role");
    }

    @PostMapping("/post")
    public HelloDto adminPostExample(@RequestBody HelloDto helloDto, Principal principal) {
        log.info("The user is {}", principal);
        return helloDto;
    }

    @GetMapping("/post")
    public String adminPostExample(Principal principal) {
        return "Hello " + principal.getName();
    }

}

package az.ingress.security.rest;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
public class SecurityConf extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable() //cors
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/secure/hello/**")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/secure/user").hasAnyRole("USER", "ADMIN") //ROLE_
                .and()
                .authorizeRequests()
                .antMatchers("/secure/admin").hasAnyRole("ADMIN")
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/secure/post").authenticated()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/secure/post").authenticated();

        super.configure(http);
    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("ilkin")
//                .password("{noop}ilkin")
//                .roles("USER")
//                .and()
//                .withUser("admin")
//                .password("{noop}admin")
//                .roles("ADMIN");
//    }

}
